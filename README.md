# EKS NODE & GO Deployment 

to start using this script you need :
- AWS Account 
- aws-cli installed on your local
- EKS running on aws

# Build Service
## if you want to make first time build the image
for GO :
- `cd app/go-hello`
- `docker build -t addityar/go-hello:latest .` 
- after build success push image to dockerhub `docker push addityar/go-hello:latest`
- your image ready to run/deploy

For Node :
- `cd app/node-hello`
- `docker build -t addityar/node-hello:latest .`
- after build success push image to dockerhub `docker push addityar/node-hello:latest`
- your image ready to run/deploy

## EKS Deployment 
deploy your images to EKS :
1. deploy your service to eks pod
for go :
- `cd k8s/deployment`
- `kubectl apply -f go-hello.yaml`

for node : 
- `cd k8s/deployment`
- `kubectl apply -f node-hello.yaml`

2. apply ingress if you want to expose http/https routes from outside cluster
- `cd k8s/ingress`
- `kubectl apply -f ingress-go-hello.yaml`
- `kubectl apply -f ingress-node-hello.yaml`
- now you can access the service. ex: node.addityar.xyz go.addityar.xyz

## Using CI/CD 
I have 2 option :

1. using gitlab-CI
- using gitlab CI with file **.gitlab-ci.yaml**
- every push your code to repo, gitlab will running automatically

2. using jenkins
- use jenkins to build and deploy with file **jenkinsfile**
- every push your code to repo, jenkins will running automatically